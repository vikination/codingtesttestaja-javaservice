public class HelloWorld{
    
     public static void main(String []args){
        System.out.println("Hello World");
        
        Service service = ServiceB.getInstance();
        System.out.println(service.b());
     }
     
}

interface Service{
    String b();
}

class ServiceA implements Service{
    
    public String b(){return "Q";}
    
    public static Service getInstance(){
        return new ServiceA();
    }
    
}

class ServiceB implements Service{
    public String b(){return "R";}
    
    public static Service getInstance(){
        return new ServiceB();
    }
}